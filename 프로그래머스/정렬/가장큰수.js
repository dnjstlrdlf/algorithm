function solution(numbers) {
    numbers.sort((a,b) => {
        a = a.toString();
        b = b.toString();
        const ab = a+b;
        const ba = b+a;
        return ba - ab;
    });
    console.log(numbers);
    return (numbers[0] == '0')?'0':numbers.join('');
}

// solution([6, 10, 2]);
solution([3, 30, 34, 5, 9]);