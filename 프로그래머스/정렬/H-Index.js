function solution(citations) {
    const citiLength = citations.length;
    citations.sort((a,b) => b-a);
    
    if (citations[0] == 0) { return 0 }

    for(let i=0; i<citiLength; i++) {
        const cnt = i+1;
        const citationCnt = citations[i];
        if (cnt >= citationCnt) {
            console.log(cnt);
            return i;
        }
    }
    
    return citiLength;
}

// solution([3, 0, 6, 1, 5]);
solution([10, 50, 100]);