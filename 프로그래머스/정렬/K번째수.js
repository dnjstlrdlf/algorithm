function solution(array, commands) {
    let answer = [];

    for(let i=0; i<commands.length; i++) {
        const startIdx = commands[i][0]-1;
        const endIdx = commands[i][1];
        const peekIdx = commands[i][2]-1;

        let splitArr = array.slice(startIdx, endIdx).sort((a,b) => a-b);
        
        answer.push(splitArr[peekIdx]);
    }

    return answer;
}

solution([1, 5, 2, 6, 3, 7, 4], [[2, 5, 3], [4, 4, 1], [1, 7, 3]]);