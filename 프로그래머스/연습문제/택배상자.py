def solution(order):
    answer = 0
    idx = 1
    order_idx = 0
    max_cnt = max(order)
    sub_container = []  # 보조 컨베이어 벨트

    # [1,2,3,4....] 순서로 메인 컨베이어 벨트
    while order:
        check_pop = False

        # 메인 컨베이어랑 일치하는 경우
        if idx == order[order_idx]:
            order_idx += 1
            answer += 1
            check_pop = True
            idx += 1

        # 보조 컨베이어랑 일치하는 경우
        elif sub_container and order[order_idx] == sub_container[len(sub_container)-1]:
            sub_container.pop()
            order_idx += 1
            answer += 1
            check_pop = True

        # 일치하지 않는 경우
        elif max_cnt >= idx:
            sub_container.append(idx)
            check_pop = True
            idx += 1

        # print(f'order:: {order}')
        # print(f'order idx:: {order_idx}')
        # print(f'sec_queue:: {sub_container}')
        # print(f'answer:: {answer}')
        # print('-----------------------------------')

        # if not check_pop and not first_queue and not sec_queue:
        if not check_pop or order_idx >= max_cnt:
            break

    return answer


print(solution([4, 3, 1, 2, 5]))
# print(solution([5, 4, 3, 2, 1]))
# print(solution([1, 2, 3, 4, 5]))
# print(solution([2, 1, 4, 3, 6, 5, 8, 7, 10, 9]))
# print(solution([2, 1, 6, 7, 5, 8, 4, 9, 3, 10]))


# 초기 => 두가지 타임아웃
# def solution(order):
#     answer = 0
#     idx = 1
#     max_cnt = max(order)
#     sub_container = []  # 보조 컨베이어 벨트
#
#     # [1,2,3,4....] 순서로 메인 컨베이어 벨트
#     while order:
#         check_pop = False
#
#         # 메인 컨베이어랑 일치하는 경우
#         if idx == order[0]:
#             order.pop(0)
#             answer += 1
#             check_pop = True
#             idx += 1
#
#         # 보조 컨베이어랑 일치하는 경우
#         elif sub_container and order[0] == sub_container[len(sub_container)-1]:
#             sub_container.pop()
#             order.pop(0)
#             answer += 1
#             check_pop = True
#
#         # 일치하지 않는 경우
#         elif max_cnt >= idx:
#             sub_container.append(idx)
#             check_pop = True
#             idx += 1
#
#
#         # if not check_pop and not first_queue and not sec_queue:
#         if not check_pop:
#             break
#
#
#     return answer