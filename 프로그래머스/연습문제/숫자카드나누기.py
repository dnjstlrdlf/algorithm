import math


def n_list_gcd(array):
    gcd = math.gcd(array[0], array[1])
    for i in range(2, len(array)):
        gcd = math.gcd(gcd, array[i])
        print(f'gcd:: {gcd}')

    return gcd


def solution(arrayA, arrayB):
    answer = 0
    a_gcd = n_list_gcd(arrayA)
    b_gcd = n_list_gcd(arrayB)

    return answer


arrayA = [14, 35, 119]
arrayB = [18, 30, 102]

print(solution(arrayA, arrayB))

print(f'test:: {math.gcd(18, 30)}')
int