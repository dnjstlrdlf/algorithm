from collections import Counter


def solution(topping):
    answer = 0
    bro_set_1 = Counter(topping)
    bro_set_2 = set()

    for cur_top in topping:
        bro_set_2.add(cur_top)
        bro_set_1[cur_top] = bro_set_1[cur_top] - 1

        if bro_set_1[cur_top] == 0:
            bro_set_1.pop(cur_top)

        if len(bro_set_1) == len(bro_set_2):
            answer += 1

    return answer


# 처음풀이 -> 시간 초과
# def solution(topping):
#     answer = 0
#
#     for i in range(1, len(topping)):
#         bef = set(topping[0:i])
#         aft = set(topping[i:])
#         print(f'bef:: {bef}')
#         print(f'aft:: {aft}')
#
#         if len(bef) == len(aft):
#             answer += 1
#
#     return answer


topping = [1, 2, 1, 3, 1, 4, 1, 2]
print(solution(topping))
