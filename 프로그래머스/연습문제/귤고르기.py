from collections import Counter


def solution(cnt, tangerine):
    # 중복제거 및 튜플 반환
    result = 0
    tangerine_cnt = Counter(tangerine).most_common()

    sum_tanger = 0
    for size, tangerine_cnt in tangerine_cnt:
        sum_tanger += tangerine_cnt
        result += 1
        if cnt <= sum_tanger:
            break

    return result


# 첫 코드 => 무조건 일치해야하는줄 알고 착각하여 작성함. 넘겨도 됨
# from itertools import combinations
# from collections import defaultdict
#
# def solution(cnt, tangerine):
#     # 중복제거
#     tangerine_set = list(set(tangerine))
#     print(tangerine_set)
#
#     # 카운팅
#     tangerine_dict = defaultdict(int)
#     for i in range(len(tangerine_set)):
#         tangerine_size = tangerine_set[i]
#         tangerine_dict[tangerine_size] = tangerine.count(tangerine_size)
#
#     print(tangerine_dict)
#
#     end_length = len(tangerine_set)+1
#     for i in range(1, end_length):
#         combi = list(combinations(tangerine_set, i))
#
#         for j in range(len(combi)):
#             sum_tangerine = 0
#             print(f'temp:: {combi}')
#
#             for k in range(len(combi[j])):
#                 cur_tangerine_size = combi[j][k]
#                 sum_tangerine += tangerine_dict[cur_tangerine_size]
#
#             print(f'combi:: {combi} / sum_tangerine:: {sum_tangerine}')
#
#             if cnt <= sum_tangerine:
#                 print(f'res:: {combi[j]}')
#                 return len(combi[j])
#
#     return 0


k = 6
tangerine = [1, 3, 2, 5, 4, 5, 2, 3]
# k = 4
# tangerine = [1, 3, 2, 5, 4, 5, 2, 3]
# k = 2
# tangerine = [1, 1, 1, 1, 2, 2, 2, 3]

print(solution(k, tangerine))