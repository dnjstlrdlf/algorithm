from collections import defaultdict
import heapq


def solution(info, edges):
    answer = 0
    graph = defaultdict(list)

    for str, end in edges:
        graph[str].append(end)

    pq = [[-1, 0, 0]]
    visit = [False] * len(info)

    while pq:
        cur_wolf, cur_sheep, cur_node = heapq.heappop(pq)
        visit[cur_node] = True

        if cur_wolf >= cur_sheep:
            cur_wolf -= 1
            visit[cur_node] = False
            continue

        print('info', cur_wolf, cur_sheep, cur_node)

        if answer < cur_sheep:
            answer = cur_sheep

        for next_node in graph[cur_node]:
            next_wolf, next_sheep = cur_wolf, cur_sheep
            if info[next_node] == 1:
                next_wolf += 1
            else:
                next_sheep += 1

            heapq.heappush(pq, [next_wolf, next_sheep, next_node])

    return answer


# info = [0,0,1,1,1,0,1,0,1,0,1,1]
# edges = [[0,1],[1,2],[1,4],[0,8],[8,7],[9,10],[9,11],[4,3],[6,5],[4,6],[8,9]]
info = [0,1,0,1,1,0,1,0,0,1,0]
edges = [[0,1],[0,2],[1,3],[1,4],[2,5],[2,6],[3,7],[4,8],[6,9],[9,10]]

print(solution(info, edges))