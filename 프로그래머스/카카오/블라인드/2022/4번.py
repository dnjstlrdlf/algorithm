from itertools import permutations, combinations


# 과녁 점수조합
def get_arrows(n):
    arrows_list = [i for i in range(1, 11)]
    save_list = []

    for i in range(1, n+1):
        temp_list = list(combinations(arrows_list, i))
        temp_list.reverse()
        save_list += temp_list

    return save_list


def make_n_list(n):
    save_list = []
    arrows_list = [i for i in range(1, n+1)]

    for i in range(1, n+1):
        temp_list = list(permutations(arrows_list, i))
        for j in range(len(temp_list)):
            if sum(temp_list[j]) == n:
                save_list.append(temp_list[j])

    return save_list


def solution(n, info):
    answer = []
    combi_list = get_arrows(n)
    print(combi_list)

    n_list = make_n_list(n)
    print('n_list', n_list)

    return answer


n, info = 5, [2,1,1,1,0,0,0,0,0,0,0]

print(solution(n, info))