from collections import defaultdict


def converse_min(car_in, car_out):
    start_hour, start_min = map(int, car_in.split(':'))
    end_hour, end_min = map(int, car_out.split(':'))
    start_total_min = 60 * start_hour + start_min
    end_total_min = 60 * end_hour + end_min
    sub_total_min = end_total_min - start_total_min

    return sub_total_min


def return_cost(cur_total_min, fees):
    total_cost = 0
    base_time, base_cost, add_time, add_cost = fees[0], fees[1], fees[2], fees[3]

    if cur_total_min > base_time:
        total_cost += base_cost
        cur_total_min -= base_time
        total_cost += (cur_total_min // add_time) * add_cost
        total_cost += add_cost if cur_total_min % add_time > 0 else 0
    else:
        total_cost = base_cost

    return total_cost


def solution(fees, records):
    answer = []
    save_info = defaultdict(list)

    for i in range(len(records)):
        time, car_no, gu = records[i].split()
        print(records[i].split())
        if gu == 'IN':
            save_info[car_no].append({
                'in': time
            })
        else:
            last_idx = len(save_info[car_no])-1
            save_info[car_no][last_idx]['out'] = time

    print(save_info)
    key_list = list(save_info.keys())
    key_list.sort()
    print(key_list)
    for key in key_list:
        cur_row = save_info[key]
        cur_total_min = 0
        print('key', key)

        for reipt in cur_row:
            if 'out' not in reipt:
                reipt['out'] = "23:59"
            cur_total_min += converse_min(reipt['in'], reipt['out'])

        if '0148' == key:
            print(cur_total_min)

        cur_cost = return_cost(cur_total_min, fees)
        answer.append(cur_cost)
    return answer


fees = [180, 5000, 10, 600]
records = [
    "05:34 5961 IN",
    "06:00 0000 IN",
    "06:34 0000 OUT",
    "07:59 5961 OUT",
    "07:59 0148 IN",
    "18:59 0000 IN",
    "19:09 0148 OUT",
    "22:59 5961 IN",
    "23:00 5961 OUT"
]

print( solution(fees, records) )