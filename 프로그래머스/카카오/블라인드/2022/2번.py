import math

def converse_n(n, q):
    rev_base = ''

    while n > 0:
        n, mod = divmod(n, q)
        rev_base += str(mod)

    return rev_base[::-1]


def is_prime_number(x):
    temp = int(math.sqrt(x))+1
    for i in range(2, temp):
        if x % i == 0:
            return False
    return True


def solution(n, k):
    answer = 0
    converse_nth = converse_n(n, k).split('0')
    temp_list = [int(converse_nth[i]) if converse_nth[i] else 0 for i in range(len(converse_nth))]

    for i in range(len(temp_list)):
        if temp_list[i] > 1 and is_prime_number(int(temp_list[i])):
            answer += 1

    return answer


n = 437674
# n = 110011
# n = 999999
# n = 1
k = 3
# k = 10
# k = 10
print(solution(n, k))

