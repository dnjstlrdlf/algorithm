from collections import defaultdict


def solution(id_list, report, k):
    answer = []
    report_history = defaultdict(list)
    report_cnt_history = defaultdict(int)

    for i in range(len(report)):
        start, end = report[i].split()

        if end not in report_history[start]:
            report_history[start].append(end)
            report_cnt_history[end] += 1

    print(report_history)
    print(report_cnt_history)

    for idx in range(len(id_list)):
        cur_list = report_history[id_list[idx]]
        cur_cnt = 0
        for his_id in cur_list:
            if report_cnt_history[his_id] >= k:
                cur_cnt += 1

        answer.append(cur_cnt)

    return answer


id_list = ["muzi", "frodo", "apeach", "neo"]
id_list = ["con", "ryan"]
report = ["muzi frodo", "apeach frodo", "frodo neo", "muzi neo", "apeach muzi"]
report = ["ryan con", "ryan con", "ryan con", "ryan con"]
k = 2
k = 3

print(solution(id_list, report, k))
