import copy

def rotateKey(key, n):
    N = len(key)
    ret = [[0] * N for _ in range(N)]

    if n % 4 == 1:
        for r in range(N):
            for c in range(N):
                ret[c][N-1-r] = key[r][c]
    elif n % 4 == 2:
        for r in range(N):
            for c in range(N):
                ret[N-1-r][N-1-c] = key[r][c]
    elif n % 4 == 3:
        for r in range(N):
            for c in range(N):
                ret[N-1-c][r] = key[r][c]
    else:
        for r in range(N):
            for c in range(N):
                ret[r][c] = key[r][c]

    return ret


def insertPadding(lock, key):
    saveLock = []
    verticalPadding = [0] * ((len(key)-1)*2 + len(lock[0]))

    for i in range(len(key)-1):
        saveLock.insert(0, verticalPadding)

    for i in range(len(lock)):
        horizenPadding = [0] * (len(key)-1)
        saveLock.append(horizenPadding + lock[i][:] + horizenPadding)

    for i in range(len(key)-1):
        saveLock.append(verticalPadding)

    return saveLock


def addArea(saveKey, saveLock, i, j):
    tempLock = copy.deepcopy(saveLock)

    # 자물쇠에 key값을 저장
    for a in range(i, len(saveKey)+i):
        for b in range(len(saveKey[0])):
            # print('i',i,'j', j,'a',a,'b',b, end=' / ')
            # print('a', a, 'b', b, end=' / ')
            tempLock[a][b+j] += saveKey[a-i][b]
        # print()

    print('saveKey---------------------------------')
    for row in saveKey:
        print(row)
    print('tempLock---------------------------------')
    for row in tempLock:
        print(row)
    print('---------------------------------')
    return tempLock


def checkArea(key, lock):
    saveKey = copy.deepcopy(key)
    saveLock = copy.deepcopy(lock)
    startIindex = len(saveKey)-1
    startJindex = len(saveKey[0])-1
    endIindex = len(saveLock)-(len(saveKey)-1)
    endJindex = len(saveLock[0]) - (len(saveKey[0]) - 1)
    collectCnt = endIindex - startIindex
    rowCheckCnt = len(saveLock[0]) - (len(saveKey[0])-1)*2

    for i in range(startIindex, endIindex):
        row = lock[i][startJindex:endJindex]
        print(row, row.count(1), len(saveLock[0]) - (len(saveKey[0])-1)*2)
        if row.count(1) == rowCheckCnt:
            collectCnt -= 1

    print('---------------------------------')
    return True if collectCnt == 0 else False


def solution(key, lock):
    lock = insertPadding(lock, key)
    jLimit = len(key)-1
    kLimit = len(key[0])-1

    for i in range(4):
        currentKey = rotateKey(key, i)

        for j in range(len(lock)-jLimit):
            for k in range(len(lock[0])-kLimit):
                currentLock = addArea(currentKey, lock, j, k)
                checked = checkArea(currentKey, currentLock)
                if checked:
                    return True

    return False


key = [[0, 0, 0], [1, 0, 0], [0, 1, 1]]
lock = [[1, 1, 1], [1, 1, 0], [1, 0, 1]]

print(solution(key, lock))