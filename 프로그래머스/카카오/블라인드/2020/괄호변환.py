def solution(w):
    return divideWord(w)


def divideWord(w):
    leftCnt, rightCnt, saveIdx = 0, 0, 0

    if w == '':
        return ''

    for i in range(len(w)):
        if w[i] == '(':
            leftCnt += 1
        else:
            rightCnt += 1

        if leftCnt == rightCnt:
            saveIdx = i
            break

    u = w[0:saveIdx+1]
    v = w[saveIdx+1:]
    print('u와 v분리', u, ' ||| ', v)
    print('check u', checkCollect(u))

    if not checkCollect(u):
        rec = divideWord(v)
        temp = '(' + rec + ')'
        u = u[1:]
        u = u[:-1]
        u = u.replace('(', '2')
        u = u.replace(')', '1')
        u = u.replace('1', '(')
        u = u.replace('2', ')')
        return temp + u
    else:
        return u+divideWord(v)




def checkCollect(u):
    stackWord = list(u)
    topWord = stackWord.pop()

    if topWord == '(':
        return False

    checkCnt = 1    # )일떄 +1 (일떄 -1

    while stackWord:
        nextWord = stackWord.pop()
        if nextWord == ')':
            checkCnt += 1
        else:
            checkCnt -= 1

        if checkCnt < 0:
            return False

    return True

# w = ')('
w = '()))((()'
# w = '(()())()'
print(solution(w))