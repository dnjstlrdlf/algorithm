# from collections import defaultdict
#
#
# class Node(object):
#     def __init__(self, key, data=None):
#         self.key = key
#         self.data = data
#         self.children = {}
#         self.length = defaultdict(int)
#
#
# class Trie:
#     def __init__(self):
#         self.head = Node(None)
#
#     def insert(self, string):
#         current_node = self.head
#         current_node.length[len(string)] += 1
#
#         for char in string:
#             if char not in current_node.children:
#                 current_node.children[char] = Node(char)
#             current_node = current_node.children[char]
#         current_node.data = string
#
#     def search(self, string):
#         current_node = self.head
#
#         for char in string:
#             if char in current_node.children:
#                 current_node = current_node.children[char]
#             else:
#                 return False
#
#         if current_node.data:
#             return True
#         else:
#             return False
#
#     def starts_with(self, prefix, word_length):
#         current_node = self.head
#         words = []
#
#         for p in prefix:
#             if p in current_node.children:
#                 current_node = current_node.children[p]
#             else:
#                 return []
#
#         current_node = [current_node]
#         next_node = []
#
#         while True:
#             for node in current_node:
#                 if node.data and len(node.data) == word_length:
#                     words.append(node.data)
#                 next_node.extend(list(node.children.values()))
#             if len(next_node) != 0:
#                 current_node = next_node
#                 next_node = []
#             else:
#                 break
#
#         return words
#
#
# def solution(words, queries):
#     queries_len = len(queries)
#     answer = [0]*queries_len
#     trie = Trie()
#     reverse_trie = Trie()
#     use_queries = defaultdict(int)
#
#     for word in words:
#         trie.insert(word)
#         reverse_trie.insert(word[::-1])
#
#     for i in range(queries_len):
#         query = queries[i]
#         search_result = 0
#
#         if query in use_queries:
#             answer[i] = use_queries[query]
#             continue
#
#         alphabet = query.replace('?', '')
#
#         if '?'*len(query) == query:
#             search_result = trie.head.length[len(query)]
#         elif query[0] == '?':
#             search_result = len(reverse_trie.starts_with(alphabet[::-1], len(query)))
#         else:
#             search_result = len(trie.starts_with(alphabet, len(query)))
#
#         answer[i] = search_result
#         use_queries[query] = search_result
#
#     return answer


import bisect
from collections import defaultdict


def solution(words, queries):
    answer = []
    forward_word = defaultdict(list)
    back_word = defaultdict(list)

    for word in words:
        word_len = len(word)
        forward_word[word_len].append(word)
        back_word[word_len].append(word[::-1])

    for f_word, b_word in zip(forward_word.values(), back_word.values()):
        f_word.sort()
        b_word.sort()

    for query in queries:
        if query[0] == '?':
            query = query[::-1]
            start_idx = bisect.bisect_left(back_word[len(query)], query.replace('?', 'a'))
            end_idx = bisect.bisect_left(back_word[len(query)], query.replace('?', 'z'))
        else:
            start_idx = bisect.bisect_left(forward_word[len(query)], query.replace('?', 'a'))
            end_idx = bisect.bisect_left(forward_word[len(query)], query.replace('?', 'z'))
        print(query[0] == '?', end_idx, start_idx)
        answer.append(end_idx - start_idx)

    return answer


words = ["frodo", "front", "frost", "frozen", "frame", "kakao"]
queries = ["k????","fro??", "????o", "fr???", "fro???", "pro?"]
# queries = ["fro??", "????o", "fr???", "fro???", "pro?"]

print(solution(words, queries))