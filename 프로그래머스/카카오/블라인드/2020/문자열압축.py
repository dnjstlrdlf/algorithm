def solution(s):
    sLen = len(s)
    answer = sLen

    for i in range(sLen):
        saveWord = ''
        saveBef = ''
        saveCnt = 0
        for j in range(0, sLen, i+1):
            current = s[j:j+i+1]
            if saveBef != current:
                saveWord += f'{saveCnt if saveCnt > 1 else ""}{saveBef}'
                saveBef = current
                saveCnt = 1
            else:
                saveCnt += 1
            print('saveWord', saveWord)

        saveWord += f'{saveCnt if saveCnt > 1 else ""}{saveBef}'
        if answer > len(saveWord):
            answer = len(saveWord)

    return answer


s = 'aabbaccc'
s = 'ababcdcdababcdcd'
s = 'abcabcdede'
s = 'abcabcabcabcdededededede'
s = 'xababcdcdababcdcd'
print(solution(s))