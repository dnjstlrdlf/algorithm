import bisect
from collections import defaultdict


# def solution(info, query):
#     answer = []
#     for i in range(len(info)):
#         temp = info[i].split()
#         info[i] = {'row':set(temp[:-1]), 'score': int(temp[-1])}
#
#     for row in query:
#         temp = row.replace('-', '').replace(' and ', ' ').split()
#         query_score = int(temp[-1])
#         set_row = set(temp[:-1])
#
#         collect_cnt = 0
#
#         print(set_row, query_score, '--------------------------------')
#         for info_row in info:
#             intersct = info_row['row'] & set_row
#             if len(intersct) == len(set_row) and info_row['score'] >= query_score:
#                 print(info_row['row'], info_row['score'], intersct, info_row['score'] >= query_score)
#                 collect_cnt += 1
#
#         print(collect_cnt)
#         answer.append(collect_cnt)
#
#     return answer


def solution(info, query):
    answer = []
    save_keys = defaultdict(list)

    for i in range(len(info)):
        temp = info[i].split()

    return answer


info = [
    "java backend junior pizza 150",
    "python frontend senior chicken 210",
    "python frontend senior chicken 150",
    "cpp backend senior pizza 260",
    "java backend junior chicken 80",
    "python backend senior chicken 50"
]
query = [
    "java and backend and junior and pizza 100",
    "python and frontend and senior and chicken 200",
    "cpp and - and senior and pizza 250",
    "- and backend and senior and - 150",
    "- and - and - and chicken 100",
    "- and - and - and - 150"
]

print(solution(info, query))