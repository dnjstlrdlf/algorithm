import heapq

INF = int(1e9)
maps = [[]]


def make_map(fares, n):
    global maps
    maps = [[] for i in range(n + 1)]

    for start, end, cost in fares:
        maps[start].append([end, cost])
        maps[end].append([start, cost])


def dijkstra(start, end):
    global maps
    n = len(maps)
    save_cost = [INF for i in range(n+1)]   # 무한으로 비용배열 초기화
    save_cost[start] = 0                    # 시작지점비용0으로 초기화
    priorityQueue = [[0, start]]        # 비용, 시작지점(현재노드)

    while priorityQueue:
        price, node = heapq.heappop(priorityQueue)  # 가장 비용이 적은것 꺼냄

        if save_cost[node] < price:     # 현재노드까지의 비용이 저장된 비용보다 비싸면 패스
            continue

        for node_info in maps[node]:
            next_node, next_price = node_info[0], node_info[1]
            next_price += price

            if next_price < save_cost[next_node]:   # 저장된 다음노드의 비용이 더 비싼경우 갱신
                save_cost[next_node] = next_price  # 최소비용 갱신
                heapq.heappush(priorityQueue, [next_price, next_node])

    return save_cost[end]


def solution(n, s, a, b, fares):
    cost = INF
    make_map(fares, n)

    for i in range(1, n+1): # 1~n까지 간선들에 대한 최소범위 계산
        # 시작점부터 임의의 간선 n까지 합승최소비용 + n부터 A도착 최소 비용 + n부터 B도착 최소 비용
        cost = min(cost, dijkstra(s, i) + dijkstra(i, a) + dijkstra(i, b))

    return cost


# n = 지점개수, s = 출발지점, a = A의 도착지점, b = B의 도착지점
n, s, a, b = 6, 4, 6, 2
fares = [[4, 1, 10], [3, 5, 24], [5, 6, 2], [3, 1, 41], [5, 1, 24], [4, 6, 50], [2, 4, 66], [2, 3, 22], [1, 6, 25]]
print(solution(n,s,a,b,fares))