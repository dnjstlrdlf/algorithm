from collections import defaultdict
from itertools import combinations


def solution(orders, course):
    order_cnt = defaultdict(dict)   # 메뉴
    answer = []

    for order in orders:
        for co in course:
            combi = list(combinations(order, co))

            for row in combi:
                combi_list = [row[i] for i in range(len(row))]
                combi_list.sort()
                combi_name = ''.join(combi_list)

                if combi_name in order_cnt[len(combi_name)]:
                    order_cnt[len(combi_name)][combi_name] += 1
                else:
                    order_cnt[len(combi_name)][combi_name] = 1

    for key in order_cnt.keys():
        order_list = sorted(order_cnt[key].items(), key= lambda row: row[1], reverse=True)
        max_cnt = 0
        for row in order_list:
            if max_cnt <= row[1] and row[1] >= 2:
                max_cnt = row[1]
                answer.append(row[0])
            else:
                break

    answer.sort()

    return answer

course = [2,3,4]
course = [2,3,5]
course = [2,3,4]
orders = ["ABCFG", "AC", "CDE", "ACDE", "BCFG", "ACDEH"]
orders = ["ABCDE", "AB", "CD", "ADE", "XYZ", "XYZ", "ACD"]
orders = ["XYZ", "XWY", "WXA"]
print(solution(orders, course))