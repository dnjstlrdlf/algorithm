import re


def converse_comma(lower_id):
    save_id = ''
    comma_cnt = 0
    for word in lower_id:
        if word == '.':
            if comma_cnt == 0:
                save_id += '.'
                comma_cnt += 1
        else:
            comma_cnt = 0
            save_id += word
    return save_id


# 1단계 변화 없습니다.
# 2단계 "=.=" → "."
# 3단계 변화 없습니다.
# 4단계 "." → "" (new_id가 빈 문자열이 되었습니다.)
# 5단계 "" → "a"
# 6단계 변화 없습니다.
# 7단계 "a" → "aaa"

def converse_id(id):
    lower_id = id.lower()
    print('1단계', lower_id)
    special = re.compile('[^a-z0-9|.|\-|_]')
    lower_id = special.sub('', lower_id)
    print('2단계', lower_id)
    remove_comma = converse_comma(lower_id)
    print('3단계', remove_comma)

    if remove_comma[0] == '.':
        remove_comma = remove_comma[1:]
    print('4-1단계', remove_comma)
    if remove_comma and remove_comma[len(remove_comma)-1] == '.':
        remove_comma = remove_comma[0:len(remove_comma)-1]
    print('4-2단계', remove_comma)

    if remove_comma == '':
        remove_comma = "a"
        print('5단계', remove_comma)

    if len(remove_comma) > 15:
        remove_comma = remove_comma[:15]
        print('6단계', remove_comma)

    if remove_comma[len(remove_comma)-1] == '.':
        remove_comma = remove_comma[:-1]
        print('7단계', remove_comma)

    if len(remove_comma) <= 2:
        last_word = remove_comma[len(remove_comma)-1]
        remove_comma = remove_comma + (last_word * (3 - len(remove_comma)))
        print('8단계', remove_comma)

    return remove_comma


def solution(new_id):
    return converse_id(new_id)


new_id = "...!@BaT#*..y.abcdefghijklm"
new_id = "z-+.^."
# new_id = "=.="
# new_id = "123_.def"
# new_id = "abcdefghijklmn.p"
print(solution(new_id))