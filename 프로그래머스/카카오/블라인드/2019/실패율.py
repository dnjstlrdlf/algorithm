from collections import Counter

def solution(N, stages):
    answer = [[0, 0] for i in range(N)]
    saveUser = 0
    totalUser = len(stages)
    stages = Counter(stages)

    for i in range(N):
        current_stage = i+1
        stage_user = stages[i+1]
        fail_percent = 0 if stage_user == 0 else stage_user / (totalUser - saveUser)
        answer[i] = [current_stage, fail_percent]
        saveUser += stage_user

    answer.sort(key=lambda row:(row[1], -row[0]), reverse=True)

    return [answer[i][0] for i in range(len(answer))]

N = 5
N = 4
stages = [2, 1, 2, 6, 2, 4, 3, 3]
stages = [4,4,4,4,4]
print(solution(N, stages))