from itertools import combinations


def solution(relation):
    useCol = [i for i in range(0, len(relation[0]))]
    uniqueColList = []

    for row in range(len(relation[0])):
        uniqueColList += combinations(useCol, row + 1)

    saveList = []

    for colTuple in uniqueColList:   # 0, ~ {0, 1}
        saveDict = dict()

        for i in range(len(relation)):
            isOverlap = False
            saveKey = ''

            for col in colTuple:
                saveKey += relation[i][col]

            if saveKey in saveDict:
                isOverlap = True
                break
            saveDict[saveKey] = 1

        if not isOverlap:
            saveList.append(set(colTuple))

    print(saveList)
    minimalList = []

    for i in range(len(saveList)):
        isSubset = False

        for j in range(len(saveList)):
            if i == j:
                continue

            if saveList[i].issuperset(saveList[j]):
                isSubset = True
                break

        if not isSubset:
            minimalList.append(saveList[i])

    print(minimalList)

    return len(uniqueColList)


relation = [
    ["100", "ryan", "music", "2"],
    ["200", "apeach", "math", "2"],
    ["300", "tube", "computer", "3"],
    ["400", "con", "computer", "4"],
    ["500", "muzi", "music", "3"],
    ["600", "apeach", "music", "2"]
]

# relation = [
#     ["100","ryan","music","2"],
#     ["200","apeach","math","2"],
#     ["300","tube","computer","3"],
#     ["400","con","computer","4"],
#     ["500","muzi","music","3"],
#     ["600","apeach","music","2"]
# ]
print(solution(relation))
