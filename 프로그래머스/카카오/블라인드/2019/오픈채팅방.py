def solution(record):
    nickDict = dict()
    messages = []
    answer = []

    for row in record:
        row = row.split()
        if len(row) == 3:
            if row[0] != 'Change' or row[0] != 'Enter':
                nickDict[row[1]] = row[2]

        if row[0] != 'Change':
            messages.append({'action': row[0], 'uid': row[1]})

    for row in messages:
        message = f'{nickDict[row["uid"]]}님이 {"들어왔습니다." if row["action"] == "Enter" else "나갔습니다."}'
        answer.append(message)

    return answer


record = ["Enter uid1234 Muzi", "Enter uid4567 Prodo","Leave uid1234","Enter uid1234 Prodo","Change uid4567 Ryan"]
print(solution(record))