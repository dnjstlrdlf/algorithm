from collections import defaultdict
import sys

sys.setrecursionlimit(10**6)

class Node:
    def __init__(self, item):
        self.val = item
        self.left = None
        self.right = None


# 이진트리 만들기
class BinaryTree:
    # 초기값 head는 None
    def __init__(self):
        self.head = Node(None)
        self.preorder_list=[]
        self.inorder_list=[]
        self.postorder_list=[]

    # 값 추가하기 head가 없을 경우
    def add(self, item):
        if self.head.val is None:
            self.head.val = item

        # head가 있으면 왼쪽배치 or 오른쪽배치
        else:
            self.__add_node(self.head, item)

    # head가 있는 경우
    def __add_node(self, cur, item):

        # head 값이 크면 왼쪽으로
        if cur.val['x'] >= item['x']:
            if cur.left is not None:
                self.__add_node(cur.left, item)
            else:
                cur.left = Node(item)
        # head 값이 작으면 오른쪽으로
        else:
            if cur.right is not None:
                self.__add_node(cur.right, item)
            else:
                cur.right = Node(item)

    # 찾기!!
    def search(self, item):
        if self.head.val is None:
            return False
        else:
            return self.__search_node(self.head, item)

    def __search_node(self, cur, item):

        if cur.val == item:
            return True
        else:
            if cur.val >= item:
                if cur.left is not None:
                    return self.__search_node(cur.left, item)
                else:
                    return False
            else:
                if cur.right is not None:
                    return self.__search_node(cur.right, item)
                else:
                    return False

    # 전위순회
    def preorder_traverse(self):
        if self.head is not None:
            self.__preorder(self.head)

    def __preorder(self, cur):
        self.preorder_list.append(cur.val['idx'])

        if cur.left is not None:
            self.__preorder(cur.left)
        if cur.right is not None:
            self.__preorder(cur.right)

    # 중위순회
    def inorder_traverse(self):
        if self.head is not None:
            self.__inorder(self.head)

    def __inorder(self, cur):
        if cur.left is not None:
            self.__inorder(cur.left)

        self.inorder_list.append(cur.val['idx'])

        if cur.right is not None:
            self.__inorder(cur.right)

    # 후위순회
    def postorder_traverse(self):
        if self.head is not None:
            self.__postorder(self.head)

    def __postorder(self, cur):
        if cur.left is not None:
            self.__postorder(cur.left)

        if cur.right is not None:
            self.__postorder(cur.right)

        self.postorder_list.append(cur.val['idx'])


def solution(nodeinfo):

    nodeInfoDict = defaultdict(list)
    for i in range(len(nodeinfo)):
        nodeInfoDict[nodeinfo[i][1]].append({'idx': i+1, 'node': {'x': nodeinfo[i][0], 'y': nodeinfo[i][1]}})

    yKey = sorted(nodeInfoDict.keys(), reverse=True)
    binaryTree = BinaryTree()

    for i in range(len(yKey)):
        nodeInfoDict[yKey[i]].sort(key=lambda row: row['node']['x'], reverse=True)
        for row in nodeInfoDict[yKey[i]]:
            binaryTree.add({'idx': row['idx'], 'y': row['node']['y'], 'x': row['node']['x']})

    binaryTree.preorder_traverse()
    binaryTree.postorder_traverse()

    return [binaryTree.preorder_list, binaryTree.postorder_list]


nodeinfo = [[5, 3], [11, 5], [13, 3], [3, 5], [6, 1], [1, 3], [8, 6], [7, 2], [2, 2]]
print(solution(nodeinfo))
