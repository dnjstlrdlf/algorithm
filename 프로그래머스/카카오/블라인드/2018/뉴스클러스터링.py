from collections import Counter


def conAlpha(str):
    strList = []
    for i in range(len(str) - 1):
        if not str[i:i + 2].isalpha():
            continue
        strList.append(str[i:i + 2].upper())
    return strList


def solution(str1, str2):
    answer = 0
    str1List = Counter(conAlpha(str1))
    str2List = Counter(conAlpha(str2))

    intersection = set(str1List) & set(str2List)
    interCnt = 0

    for key in intersection:
        minCnt = min(str1List[key], str2List[key])
        interCnt += minCnt

    union = set(str1List) | set(str2List)
    unionCnt = 0

    for key in union:
        maxCnt = max(str1List[key], str2List[key])
        unionCnt += maxCnt

    if len(union) == 0 and len(intersection) == 0:
        return 65536

    return int(interCnt / unionCnt * 65536)