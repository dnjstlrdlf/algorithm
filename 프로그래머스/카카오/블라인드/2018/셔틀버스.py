import datetime

def solution(n, t, m, timetable):
    answer = ''
    timetable.sort()
    endDrive = False

    for i in range(n):
        addMinute = t * i
        busTime = datetime.datetime(2019, 1, 1, 9 + (addMinute // 60), addMinute % 60)
        maxHuman = m

        while not endDrive:
            temp = timetable[0].split(":")
            currentTime = datetime.datetime(2019, 1, 1, int(temp[0]), int(temp[1]))

            # 종료조건
            if n == 0 and (maxHuman == 0 or currentTime > busTime):
                answer = currentTime
                endDrive = True
            elif not timetable:
                answer = busTime
                endDrive = True

            # 다음조건
            if maxHuman == 0 or currentTime > busTime:
                n -= 1
                continue

            timetable.pop(0)
            maxHuman -= 1
            print('탑승', temp, 'busTime', busTime, '탑승수', maxHuman)

        if endDrive:
            break

    print('busTime', answer)
    return answer

n, t, m = 1, 1, 5   # n회 t분 간격, m 승차 정원원
# timetable = ["08:00", "08:01", "08:02", "08:03"]
# timetable = ["23:59","23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59", "23:59"]
timetable = ["09:10", "09:09", "08:00"]
solution(n, t, m, timetable)