def solution(dartResult):
    answer = 0
    tempList = []
    currentScore = ''

    for i in range(len(dartResult)):
        current = dartResult[i]

        if current.isdecimal():
            currentScore += current

        elif current == 'D' or current == 'T' or current == 'S':
            squard = 3 if current == 'T' else 2 if current == 'D' else 1
            currentScore = int(currentScore) ** squard
            tempList.append(currentScore)
            currentScore = ''

        elif current == '*':
            for j in range(len(tempList)-1, len(tempList)-3, -1):
                if j < 0:
                    break
                tempList[j] *= 2
        elif current == '#':
            tempList[len(tempList)-1] *= -1

        print(tempList)
    return answer


dartResult = '1S*2T*3S'
solution(dartResult)
