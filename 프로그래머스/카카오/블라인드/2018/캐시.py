def solution(cacheSize, cities):
    answer = 0
    cacheList = []
    # LRU 최근에 사용하지 않은것 갱신

    for city in cities:
        city = city.upper()

        if cacheSize == 0: #
            answer += 5
        elif cacheSize > len(cacheList):
            if city not in cacheList:
                answer += 5
            else:
                answer += 1
                cacheList.remove(city)

            cacheList.append(city)
        else:
            if city not in cacheList:
                answer += 5
                cacheList.pop(0)
            else:
                cacheList.remove(city)
                answer += 1

            cacheList.append(city)


    print(answer)
    return answer

cacheSize = 5
cities = ["Jeju", "Pangyo", "Seoul", "NewYork", "LA", "Jeju", "Pangyo", "Seoul", "NewYork", "LA"]
cities = ["Jeju", "Pangyo", "Seoul", "Jeju", "Pangyo", "Seoul", "Jeju", "Pangyo", "Seoul"]
cities = ["Jeju", "Pangyo", "Seoul", "NewYork", "LA", "SanFrancisco", "Seoul", "Rome", "Paris", "Jeju", "NewYork", "Rome"]
cities = ["Jeju", "Pangyo", "Seoul", "NewYork", "LA", "SanFrancisco", "Seoul", "Rome", "Paris", "Jeju", "NewYork", "Rome"]
# cities = ["Jeju", "Pangyo", "Seoul", "NewYork", "LA"]
# cities = ["Jeju", "Pangyo", "NewYork", "newyork"]
solution(cacheSize, cities)