def checkBlock(board, y, x):
    saveIdx = []
    bombCnt = 0

    for i in range(y-1):
        for j in range(x-1):
            cntBlock = 0
            for k in range(0, 4):
                if board[i][j] == '!' or board[i][j] != board[i+(k//2)][j+(k%2)]:
                    break
                cntBlock += 1
            if cntBlock == 4:
                for k in range(0, 4):
                    saveIdx.append([i+(k//2), j+(k%2)])

    for bomb in saveIdx:
        if board[bomb[0]][bomb[1]] == '!':
            continue
        board[bomb[0]][bomb[1]] = '!'
        bombCnt += 1

    return bombCnt, board


def moveBlock(board, y, x): # m n / 4 5
    rotateBoard = []

    for i in range(x):
        saveRow = ''

        for j in range(y):
            if board[j][i] == '!':
                continue
            saveRow += board[j][i]

        if len(saveRow) < y:
            saveRow = '!'*(y-len(saveRow)) + saveRow

        moveCol = list(saveRow)
        rotateBoard.append(moveCol)

    conboard = []

    for i in range(len(rotateBoard[0])):
        saveRow = ''
        for j in range(len(rotateBoard)):
            saveRow += rotateBoard[j][i]
        conboard.append(list(saveRow))
    return conboard

def solution(m, n, board):
    answer = 0
    for i in range(m):
        board[i] = list(board[i])

    while True:
        bombCnt, board = checkBlock(board, m, n)
        answer += bombCnt

        if bombCnt == 0:
            break

        board = moveBlock(board, m, n)
    return answer

m = 6
n = 6
board = [
    "CCBDE",
    "AAADE",
    "AAABF",
    "CCBBF"]
board = ["TTTANT", "RRFACC", "RRRFCC", "TRRRAA", "TTMMMF", "TMMTTJ"]
solution(m, n, board)