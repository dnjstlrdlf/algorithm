def solution(n, arr1, arr2):
    answer = [''] * len(arr1)

    for i in range(len(arr1)):
        row = ''
        arr1Row = bin(arr1[i])[2:]
        arr1Row = '0'*(n-len(arr1Row)) + arr1Row
        arr2Row = bin(arr2[i])[2:]
        arr2Row = '0' * (n - len(arr2Row)) + arr2Row

        for j in range(len(arr1Row)):
            if arr1Row[j] == '1' or arr2Row[j] == '1':
                row += '#'
            else:
                row += ' '

        answer[i] = row

    return answer

# arr1 = [9, 20, 28, 18, 11]
arr1 = [46, 33, 33 ,22, 31, 50]
arr2 = [27 ,56, 19, 14, 14, 10]
# arr2 = [30, 1, 21, 17, 28]
# n = 5
n = 6

solution(n, arr1, arr2)