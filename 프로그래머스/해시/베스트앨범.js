function solution(genres, plays) {
    let answer = [];
    let saveObj = {};

    for(let i=0; i<genres.length; i++) {
        const gen = genres[i];
        const play = plays[i];

        if (!saveObj.hasOwnProperty(gen)) {
            saveObj[gen] = {
                total : play,
                list : [ {genNo: i, play: play} ]
            };
        } else {
            saveObj[gen].total += play;
            saveObj[gen].list.push( {genNo: i, play: play} );
        }
    }

    const keyList = Object.keys(saveObj);
    let saveArr = [];

    for(let i=0; i<keyList.length; i++) {
        saveArr.push ({
            gen: keyList[i],
            total: saveObj[keyList[i]].total,
            list: saveObj[keyList[i]].list
        });
    }

    saveArr.sort((a,b) => { return (a.total > b.total) ? -1 : (a.total < b.total) ? 1 : 0; });

    for(let i=0; i<saveArr.length; i++) {
        saveArr[i].list.sort((a,b) => { return (a.play > b.play) ? -1 : (a.play < b.play) ? 1 : 0; });
        const arr = saveArr[i].list;
        const maxLen = (arr.length >= 2) ? 2 : arr.length;
        
        for(let j=0; j<maxLen; j++) {
            answer.push(arr[j].genNo);
        }
    }

    return answer;
}


// index가 노래의 고유번호
// 장르마다 2개씩
const genres = ["classic", "pop", "classic", "classic", "pop"];
const plays = [500, 600, 150, 800, 2500];

solution(genres, plays);