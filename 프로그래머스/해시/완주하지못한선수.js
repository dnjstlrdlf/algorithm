// function solution(participant, completion) {
//     let participantOjb = {};
    
//     for(const name of participant) {
//         if (participantOjb.hasOwnProperty(name)) {
//             participantOjb[name]++;
//         } else {
//             participantOjb[name] = 1;
//         }
//     }

//     for(const compleName of completion) {
//         participantOjb[compleName]--;

//         if (participantOjb[compleName] === 0) {
//             delete participantOjb[compleName];
//         }
//     }

//     return Object.keys(participantOjb)[0];
// }
function solution(participant, completion) {
    let participantOjb = {};
    
    for(let i=0; i<participant.length; i++) {
        const name = participant[i];
        if (participantOjb.hasOwnProperty(name)) {
            participantOjb[name]++;
        } else {
            participantOjb[name] = 1;
        }
    }

    for(let i=0; i<completion.length; i++) {
        const name = completion[i];
        participantOjb[name]--;

        if (participantOjb[name] === 0) {
            delete participantOjb[name];
        }
    }

    return Object.keys(participantOjb)[0];
}

const participant = ["leo", "kiki", "eden"];
const completion = ["eden", "kiki"];

console.log(solution(participant,completion));