function solution(clothes) {
    let cnt = 1;
    let saveMap = {};

    for(let i=0; i<clothes.length; i++) {
        let clothKind = clothes[i][1];

        if (saveMap.hasOwnProperty(clothKind)) {
            saveMap[clothKind]++;
        } else {
            saveMap[clothKind] = 1;
        }
        
    }

    Object.keys(saveMap).forEach( (val, idx) => {
        cnt = cnt*(saveMap[val]+1);
    });
    
    return cnt-1;
}

const clothes = [["yellowhat", "headgear"], ["bluesunglasses", "eyewear"], ["green_turban", "headgear"]];
// const clothes = [["crowmask", "face"], ["bluesunglasses", "face"], ["smoky_makeup", "face"]];

solution(clothes);