def solution(nums):
    total_cnt = len(nums)
    set_nums = set(nums)

    print(total_cnt)
    print(set_nums)

    return len(set_nums) if total_cnt / 2 >= len(set_nums) else int(total_cnt / 2)


print(solution([3,1,2,3]))