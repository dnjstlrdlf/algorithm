function solution(progresses, speeds) {
    let answer = [];
    
    while(progresses.length>0) {
        let successCnt = 0;

        for(let i=0; i<progresses.length; i++) {
            progresses[i] = progresses[i]+speeds[i];
        }

        while(progresses.length>0) {
            if (progresses[0] >= 100) {
                progresses.shift();
                speeds.shift();
                successCnt++;
            } else { 
                break;
            }
        }

        if (successCnt>0) {
            answer.push(successCnt);
        }
    }

    console.log(answer);
    return answer;
}

// const progresses = [93, 30, 55];
// const speeds = [1, 30, 5];

const progresses = [95, 90, 99, 99, 80, 99];
const speeds = [1, 1, 1, 1, 1, 1];

solution(progresses, speeds);