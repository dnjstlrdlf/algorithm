function solution(bridge_length, weight, truck_weights) {
    let time = 0;
    let saveWeight = weight;
    let bridge = Array.from({length: bridge_length}, () => 0);
    
    while(true) {
        time++;

        // 현재 다리의 맨앞에 있는 차량 통과
        const passInfo = bridge.shift();
        weight+=passInfo;
        
        if (passInfo > 0) {
            bridge_length++;
        }
        
        // 트럭이 다리에 진입하는 조건
        if (weight >= truck_weights[0] && bridge_length>0) {
            bridge.push(truck_weights[0]);
            bridge_length--;
            weight-= truck_weights.shift();
        } else {
            bridge.push(0);
        }

        if (truck_weights.length == 0 && saveWeight == weight) {
            break;
        }
    }

    return time;
}

const bridge_length = 2;
const weight = 10;
const truck_weights = [7,4,5,6];
console.log(solution(bridge_length, weight, truck_weights));