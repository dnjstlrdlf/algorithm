function solution(priorities, location) {
    let printCnt = 0;
    let highest = priorities.slice();
    const targetPri = priorities[location];
    highest.sort();
    highest.reverse();
    let conPriorities = [];

    for(let i=0; i<priorities.length; i++) {
        conPriorities[i] = {
            idx: i,
            priority: priorities[i]
        }
    }

    while(conPriorities.length>0) {
        const { idx, priority} = conPriorities.shift();

        if (highest[0] == priority) {
            printCnt++;
            highest.shift();

            if (targetPri == priority && location == idx) {
                break;
            }

        } else {
            conPriorities.push({
                idx: idx,
                priority: priority
            });
        }
    }

    return printCnt;
}

const priorities = [2, 1, 3, 2];
const location = 2;

// const priorities = [1, 1, 9, 1, 1, 1];
// const location = 0;
console.log(solution(priorities, location));