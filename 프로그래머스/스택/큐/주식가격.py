def solution(prices):
    answer = []
    length = len(prices)

    for i in range(length):
        current = prices[i]
        time = 0
        print(current)

        for j in range(i, length-1):
            if prices[j] >= current:
                time += 1
            else:
                print(current, prices[j])
                break

        answer.append(time)

    print(answer)
    return answer


prices = [1, 2, 3, 2, 3]    # answer = [4, 3, 1, 1, 0]
solution(prices)