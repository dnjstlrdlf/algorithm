import bisect

n = int(input())
a = list(map(int, input().split()))
a.sort()
m = int(input())
b = list(map(int, input().split()))


def binary_search(target):
    left = 0
    right = len(a) - 1

    while left <= right:
        mid = (left+right)//2

        if a[mid] == target:
            return 1
        elif a[mid] > target:
            right = mid - 1
        else:
            left = mid + 1

    return 0


for num in b:
    # print(binary_search(num))
    # print(bisect.bisect_left(a, num))
    print(bisect.bisect(a, num))