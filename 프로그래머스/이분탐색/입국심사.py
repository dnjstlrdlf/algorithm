import sys


def solution(n, times):
    answer = sys.maxsize
    left, right = 1, max(times) * n

    while left <= right:
        center = (left + right) // 2
        cnt = 0

        for time in times:
            cnt += center // time

            if cnt >= n:
                break

        if cnt >= n:
            if answer > center:
                answer = center
            right = center - 1  # 단위 조절하여 최소화
        else:
            left = center + 1

    return answer


n, times = 6, [7, 10]
print(solution(n, times))