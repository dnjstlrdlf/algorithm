class Heap {
    constructor() {
      this.heap = []
    }
  
    getLeftChildIndex = (parentIndex) => parentIndex * 2 + 1
    getRightChildIndex = (parentIndex) => parentIndex * 2 + 2
    getParentIndex = (childIndex) => Math.floor((childIndex - 1) / 2)
  
    peek = () => this.heap[0] // 항상 최상위 노드가 peek 가 된다.

    insert = (key, value) => { // 우선순위를 비교하기 위해서 key, value 로 받는다.
        const node = { key, value } // 객체로 node 를 만들고
        this.heap.push(node) // push 한다.
        this.heapifyUp() // 배열에 가장 끝에 넣고, 다시 min heap 의 형태를 갖추도록 한다.
    }

    heapifyUp = () => {
        let index = this.heap.length - 1 // 계속해서 변하는 index 값
        const lastInsertedNode = this.heap[index]
    
        // 루트노드가 되기 전까지
        while (index > 0) {
          const parentIndex = this.getParentIndex(index)
    
          // 부모 노드의 key 값이 마지막에 삽입된 노드의 키 값 보다 크다면
          // 부모의 자리를 계속해서 아래로 내린다.
          if (this.heap[parentIndex].key > lastInsertedNode.key) {
            this.heap[index] = this.heap[parentIndex]
            index = parentIndex
          } else break
        }
    
        // break 를 만나서 자신의 자리를 찾은 상황
        // 마지막에 찾아진 곳이 가장 나중에 들어온 노드가 들어갈 자리다.
        this.heap[index] = lastInsertedNode
    }

    remove = () => {
        const count = this.heap.length
        const rootNode = this.heap[0]
    
        if (count <= 0) return undefined
        if (count === 1) this.heap = []
        else {
          this.heap[0] = this.heap.pop() // 끝에 있는 노드를 부모로 만들고
          this.heapifyDown() // 다시 min heap 의 형태를 갖추도록 한다.
        }
    
        return rootNode
    }

      // 변경된 루트노드가 제 자리를 찾아가도록 하는 메소드
    heapifyDown = () => {
        let index = 0
        const count = this.heap.length
        const rootNode = this.heap[index]

        // 계속해서 left child 가 있을 때 까지 검사한다.
        while (this.getLeftChildIndex(index) < count) {
            const leftChildIndex = this.getLeftChildIndex(index)
            const rightChildIndex = this.getRightChildIndex(index)

            // 왼쪽, 오른쪽 중에 더 작은 노드를 찾는다
            // rightChild 가 있다면 key의 값을 비교해서 더 작은 값을 찾는다.
            // 없다면 leftChild 가 더 작은 값을 가지는 인덱스가 된다.
            const smallerChildIndex =
                rightChildIndex < count && this.heap[rightChildIndex].key < this.heap[leftChildIndex].key
                ? rightChildIndex
                : leftChildIndex

            // 자식 노드의 키 값이 루트노드보다 작다면 위로 끌어올린다.
            if (this.heap[smallerChildIndex].key <= rootNode.key) {
                this.heap[index] = this.heap[smallerChildIndex]
                index = smallerChildIndex
            } else {
                break;
            }
        }

        this.heap[index] = rootNode
    }
}

class PriorityQueue extends Heap {
    constructor() {
      super()
    }
  
    enqueue = (priority, value) => this.insert(priority, value)
    dequeue = () => this.remove()
    isEmpty = () => this.heap.length <= 0
}


function solution(jobs) {
  jobs.sort( (a, b) => a[0] - b[0]);
  let jobsLength = jobs.length;
  let currentTime = 0;
  let totalWorkTime = 0;
  let saveQueue = new PriorityQueue();

  while(true) {
    while(jobs.length>0) {
      let reqJobTime = jobs[0][0];
      let endWorkTime = jobs[0][1];
      
      if (currentTime>=reqJobTime) {
        saveQueue.enqueue(endWorkTime, reqJobTime);
        console.log(`큐 넣기 ${jobs[0]}`);
        jobs.shift();
      } else {
        break;
      }
    }

    if (!saveQueue.isEmpty()) {
      const topWorkInfo = saveQueue.peek();
      const reqTime = topWorkInfo.value;
      const jobTime = topWorkInfo.key;
      totalWorkTime += jobTime + currentTime - reqTime;
      saveQueue.dequeue();
      currentTime+=jobTime;
    } else {
      currentTime++;
    }
    
    
    if (jobs.length===0&&saveQueue.isEmpty()) { break; }
  }

  return Math.floor(totalWorkTime/jobsLength);
}

// console.log(solution([[0, 3], [1, 9], [2, 6]]))               // 9
// console.log(solution([[0, 3], [4, 3], [10, 3]]))              // 3
// console.log(solution([[0, 10], [2, 3], [9, 3]]))              // 9
// console.log(solution([[1, 10], [3, 3], [10, 3]]))             // 9
// console.log(solution([[0, 10]]))                              // 10
// console.log(solution([[0, 10], [4, 10], [5, 11], [15, 2]]))   // 15
console.log(solution([[24, 10], [18, 39], [34, 20], [37, 5], [47, 22], [20, 47], [15, 2], [15, 34], [35, 43], [26, 1]]))   // 74
// console.log(solution([[24, 10], [18, 39], [34, 20], [37, 5], [47, 22], [20, 47], [15, 34], [15, 2], [35, 43], [26, 1]]))   // 74