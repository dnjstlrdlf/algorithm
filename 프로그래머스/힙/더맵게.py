import heapq


def solution(scoville, K):
    answer = 0
    heapq.heapify(scoville)

    while True:
        if scoville[0] >= K: # 모든 요소들이 스코빌지수 충족
            break

        if len(scoville) <= 1: # 길이가 1남았을떄
            answer = -1
            break

        firstNum = heapq.heappop(scoville)
        secondNum = heapq.heappop(scoville)
        answer += 1

        heapq.heappush(scoville, firstNum+(secondNum*2))

    print(answer)
    return answer


scoville = [1, 2, 3, 9, 10, 12];
K = 7

solution(scoville, K)
