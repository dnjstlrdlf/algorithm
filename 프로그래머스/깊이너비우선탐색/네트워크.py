def solution(n, computers):
    answer = 0
    countCom = [False] * n

    for i in range(n):
        if countCom[i]:
            continue

        netStack = [i]  # idx 저장

        while True:
            countCom[i] = True
            popIdx = netStack.pop()  # 최근 idx pop
            print('pop idx', popIdx)
            for j in range(n):
                if popIdx != j and computers[popIdx][j] == 1 and not countCom[j]:
                    countCom[j] = True
                    netStack.append(j)
                    print('append j', j)

            print('netStack', netStack)
            if len(netStack) == 0:
                break

        answer += 1
    print(answer)
    return answer


n = 3
# computers = [[1, 1, 0], [1, 1, 0], [0, 0, 1]]
computers = [[1, 1, 0], [1, 1, 1], [0, 1, 1]]


solution(n, computers)
