def wordCompare(com1, com2):
    wordCnt = len(com1)

    for pair in zip(com1, com2):
        if pair[0] == pair[1]:
            wordCnt -= 1

    return True if wordCnt == 1 else False

def solution(begin, target, words):
    answer = 0
    wordsLength = len(words)
    saveWordQueue = [{'cnt': 0, 'word': begin}]

    while True:
        info = saveWordQueue.pop()

        for word in words:
            comResult = wordCompare(info['word'], word)

            if comResult:
                appendInfo = {'cnt': info['cnt']+1, 'word': word}
                saveWordQueue.append(appendInfo)

                if target == word:
                    answer = appendInfo['cnt']
                    break
        if answer != 0 or info['cnt'] > wordsLength or len(saveWordQueue) == 0:
            break

    print(answer)

    return answer


begin = "hit"
target = "cog"
words = ["hot", "dot", "dog", "lot", "log", "cog"];
# words = ["hot", "dot", "dog", "lot", "log"]

solution(begin, target, words)