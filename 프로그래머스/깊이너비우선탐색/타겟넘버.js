function solution(numbers, target) {
    let answer = 0;
    let saveArr = [{loopCnt: 0, num:0}];
    const numArrLeng = numbers.length;

    while(saveArr.length>0) {
        const popRow = saveArr.pop();
        const cnt = popRow.loopCnt;

        if (popRow.loopCnt < numArrLeng) {
            saveArr.push({loopCnt: cnt+1, num:popRow.num+numbers[cnt]});
            saveArr.push({loopCnt: cnt+1, num:popRow.num-numbers[cnt]});
        } else if (popRow.loopCnt === numArrLeng) {
            if (target === popRow.num) {
                answer++;
            }
        }
    }
    console.log(answer);
    return answer;
}

solution([1, 1, 1, 1, 1], 3);