from collections import defaultdict


def solution(tickets):
    answer = []
    ticketDict = defaultdict(list)

    for ticket in tickets:
        ticketDict[ticket[0]].append(ticket[1])

    for key in ticketDict.keys():
        ticketDict[key].sort(reverse=True)

    stack = ['ICN']

    while stack:
        top = stack[-1]

        if not ticketDict[top]:
            answer.append(stack.pop())
        else:
            stack.append(ticketDict[top].pop())

    answer.reverse()
    print(answer)
    return answer


# tickets = [["ICN", "JFK"], ["HND", "IAD"], ["JFK", "HND"]]
tickets = [["ICN", "SFO"], ["ICN", "ATL"], ["SFO", "ATL"], ["ATL", "ICN"], ["ATL", "SFO"]]

solution(tickets)