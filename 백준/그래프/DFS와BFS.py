from collections import defaultdict


def bfs(graph, start, n):
    visit = [False] * (n+1)
    visit[start] = True
    bfs_list = [start]
    save_list = []

    while bfs_list:
        last_node = bfs_list.pop(0)
        save_list.append(last_node)
        next_node_list = graph[last_node]

        for i in range(1, len(next_node_list)):
            node = next_node_list[i]
            if not visit[i] and node == 1:
                visit[i] = True
                bfs_list.append(i)

    return save_list


def dfs(graph, start, n):
    save_list = []
    visit = [False] * (n+1)
    visit[start] = True
    dfs_list = [start]

    while dfs_list:
        last_node = dfs_list.pop()
        save_list.append(last_node)
        next_node_list = graph[last_node]

        for i in range(1, len(next_node_list)):
            node = next_node_list[i]
            if not visit[i] and node == 1:
                visit[i] = True
                dfs_list.append(i)
                break

    return save_list


n, c, start = map(int, input().split())
graph = [[0]*(n+1) for i in range(n+1)]

for i in range(c):
    key, val = map(int, input().split())
    graph[key][val] = 1
    graph[val][key] = 1

print(' '.join(map(str, dfs(graph, start, n))))
print(' '.join(map(str, bfs(graph, start, n))))