n = int(input())
maps = [list(map(int, input())) for i in range(n)]
answer = []


def check_area(y, x):
    check_list = []

    if y-1 >= 0 and maps[y-1][x] == 1:
        check_list.append([y-1, x])
    if y+1 < len(maps) and maps[y+1][x] == 1:
        check_list.append([y+1, x])
    if x-1 >= 0 and maps[y][x-1] == 1:
        check_list.append([y, x-1])
    if x+1 < len(maps[0]) and maps[y][x+1] == 1:
        check_list.append([y, x+1])

    return check_list


def search_area(y, x):
    queue = [[y, x]]
    maps[y][x] = 0
    cnt = 1

    while queue:
        current_y, current_x = queue.pop()
        next_list = check_area(current_y, current_x)
        cnt += len(next_list)

        for next_y, next_x in next_list:
            maps[next_y][next_x] = 0
            queue.append([next_y, next_x])

    return cnt


for i in range(len(maps)):
    for j in range(len(maps[0])):
        if maps[i][j] == 1:
            current_cnt = search_area(i, j)
            answer.append(current_cnt)

answer.sort()
answer = [len(answer)] + answer

for row in answer:
    print(row)