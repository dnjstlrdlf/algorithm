def check_list(y, x):
    global miro
    save_list = []

    if y-1 >= 0 and miro[y-1][x] == 1:
        save_list.append([y-1, x])
    if x-1 >= 0 and miro[y][x-1] == 1:
        save_list.append([y, x-1])
    if y+1 < len(miro) and miro[y+1][x] == 1:
        save_list.append([y+1, x])
    if x+1 < len(miro[0]) and miro[y][x+1] == 1:
        save_list.append([y, x+1])

    return save_list


n, m = map(int, input().split())
miro = [[] for row in range(n)]
queue = [[0, 0, 1]]
answer = 0

for i in range(n):
    miro[i] = list(map(int, input()))

while queue:
    current = queue.pop(0)
    miro[current[0]][current[1]] = 0
    moveable_location = check_list(current[0], current[1])

    if n-1 == current[0] and m-1 == current[1]:
        answer = current[2]
        break

    print('current', current)
    print('moveable_location', moveable_location)

    for row in miro:
        print(row)

    for move in moveable_location:
        if miro[move[0]][move[1]] == 1:
            miro[move[0]][move[1]] = 0
            queue.append(move + [current[2] + 1])

print(answer)