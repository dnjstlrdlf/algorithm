from itertools import combinations
import copy


def get_virus_location():
    global maps
    save_virus_locations = []

    for i in range(len(maps)):
        for j in range(len(maps[0])):
            if maps[i][j] == 2:
                save_virus_locations.append([i, j])

    return save_virus_locations


def get_combi_list():
    empty_list = []

    for i in range(len(maps)):
        for j in range(len(maps[0])):
            if maps[i][j] == 0:
                empty_list.append([i, j])

    return list(combinations(empty_list, 3))


def check_area(chk_map, chk_y, chk_x):
    check_list = []

    if chk_y - 1 >= 0 and chk_map[chk_y - 1][chk_x] == 0:
        check_list.append([chk_y - 1, chk_x])
    if chk_y + 1 < len(chk_map) and chk_map[chk_y + 1][chk_x] == 0:
        check_list.append([chk_y + 1, chk_x])
    if chk_x - 1 >= 0 and chk_map[chk_y][chk_x - 1] == 0:
        check_list.append([chk_y, chk_x - 1])
    if chk_x + 1 < len(chk_map[0]) and chk_map[chk_y][chk_x + 1] == 0:
        check_list.append([chk_y, chk_x + 1])

    return check_list


def virus_diffusion(locations, make_wall_maps):

    for i in range(len(locations)):
        queue = [[locations[i][0], locations[i][1]]]

        while queue:
            current_virus = queue.pop(0)
            diffusion_list = check_area(make_wall_maps, current_virus[0], current_virus[1])

            for dif_y, dif_x in diffusion_list:
                make_wall_maps[dif_y][dif_x] = 2
                queue.append([dif_y, dif_x])

    return make_wall_maps


y, x = map(int, input().split())
maps = [list(map(int, input().split())) for i in range(y)]
virus_locations = get_virus_location()
combi_list = get_combi_list()
answer = 0

for combi in combi_list:    # (1, 2, 3), (1, 2, 4) ...
    temp_map = copy.deepcopy(maps)
    for combi_y, combi_x in combi:    # ([0, 1], [0, 2], [0, 3])
        temp_map[combi_y][combi_x] = 1

    virus_diffusion_maps = virus_diffusion(virus_locations, temp_map)
    safe_area_cnt = 0

    for row in virus_diffusion_maps:
        safe_area_cnt += row.count(0)

    if safe_area_cnt > answer:
        answer = safe_area_cnt

print(answer)