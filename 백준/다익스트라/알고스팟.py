import heapq
import sys

m, n = map(int, sys.stdin.readline().split())
maps = [[] for i in range(n)]

for i in range(n):
    maps[i] = list(sys.stdin.readline())


def check_area(chk_y, chk_x):
    check_list = []

    if chk_y - 1 >= 0 and maps[chk_y - 1][chk_x] != '2':
        check_list.append([chk_y - 1, chk_x])

    if chk_y + 1 < n and maps[chk_y + 1][chk_x] != '2':
        check_list.append([chk_y + 1, chk_x])

    if chk_x - 1 >= 0 and maps[chk_y][chk_x-1] != '2':
        check_list.append([chk_y, chk_x - 1])

    if chk_x + 1 < m and maps[chk_y][chk_x+1] != '2':
        check_list.append([chk_y, chk_x + 1])

    return check_list


def dijkstra():
    pq = [[0, 0, 0]]
    node_list = [[sys.maxsize] * m for _ in range(n)]

    while pq:
        cur_beak_cnt, cur_y, cur_x = heapq.heappop(pq)
        maps[cur_y][cur_x] = '2'

        if cur_y == n-1 and cur_x == m-1:
            break

        next_list = check_area(cur_y, cur_x)

        for next_y, next_x in next_list:
            cnt = cur_beak_cnt + 1 if maps[next_y][next_x] == '1' else cur_beak_cnt

            if node_list[next_y][next_x] > cnt:
                node_list[next_y][next_x] = cnt
                heapq.heappush(pq, [cnt, next_y, next_x])

    return node_list[n-1][m-1] if node_list[n-1][m-1] != sys.maxsize else 0


print(dijkstra())