import heapq
from collections import defaultdict
import sys

INF = sys.maxsize
n = int(input())
bus_cnt = int(input())
graph = defaultdict(list)

for i in range(bus_cnt):
    start, end, cost = map(int, sys.stdin.readline().split())
    graph[start].append((cost, end))

start_city, end_city = map(int, input().split())


def dijkstra(st, ed):
    pq = [(0, st)]
    bus_costs = [INF] * (n + 1)
    bus_costs[st] = 0

    while pq:
        cur_cost, cur_city = heapq.heappop(pq)

        if bus_costs[cur_city] < cur_cost:
            continue

        for next_cost, next_city in graph[cur_city]:
            next_cost += cur_cost

            if next_cost < bus_costs[next_city]:
                bus_costs[next_city] = next_cost
                heapq.heappush(pq, (next_cost, next_city))

    return bus_costs[ed]


print(dijkstra(start_city, end_city))