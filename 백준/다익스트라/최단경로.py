from collections import defaultdict
import heapq
import sys


v, e = map(int, input().split())
starting = int(input())
maps = defaultdict(list)

INF = 10 * v + 1
distances = [INF] * (v+1)

for i in range(e):
    start, end, distance = map(int, sys.stdin.readline().split())
    maps[start].append([distance, end])


def dijkstra(input_start):
    pq = []
    heapq.heappush(pq, [0, input_start])
    distances[input_start] = 0

    while pq:
        cur_distance, cur_node = heapq.heappop(pq)

        if cur_distance > distances[cur_node]:
            continue

        for next_distace, next_node in maps[cur_node]:
            next_distace += cur_distance

            if next_distace < distances[next_node]:
                distances[next_node] = next_distace
                heapq.heappush(pq, [next_distace, next_node])


dijkstra(starting)

for i in range(1, v+1):
    print('INF' if distances[i] == INF else distances[i])