import heapq
import sys

cardBundleCnt = int(input())
priorityQueue = []
answer = 0

for i in range(cardBundleCnt):
    bundle = int(sys.stdin.readline())
    heapq.heappush(priorityQueue, bundle)

while priorityQueue:
    firstBundle = heapq.heappop(priorityQueue)

    if len(priorityQueue) == 0:
        break

    secondBundle = heapq.heappop(priorityQueue)
    addBundle = firstBundle + secondBundle
    answer += addBundle
    heapq.heappush(priorityQueue, addBundle)

print(answer)