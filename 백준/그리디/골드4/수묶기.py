import heapq

loopCnt = int(input())
positiveNum = []
negativeNum = []
answer = 0

for i in range(loopCnt):
    num = int(input())
    if num > 0:
        heapq.heappush(positiveNum, (-num, num))
    else:
        heapq.heappush(negativeNum, (num, num))

while len(positiveNum) >= 2:
    first = heapq.heappop(positiveNum)[1]
    second = heapq.heappop(positiveNum)[1]

    if first == 1 or second == 1:
        answer += first + second
    else:
        answer += first * second

if len(positiveNum) > 0:
    answer += heapq.heappop(positiveNum)[1]

while len(negativeNum) >= 2:
    first = heapq.heappop(negativeNum)[1]
    second = heapq.heappop(negativeNum)[1]
    answer += first * second

if len(negativeNum) > 0:
    answer += heapq.heappop(negativeNum)[1]

print(answer)