from collections import defaultdict

answer = 0
wordCnt = int(input())
saveWordDigit = defaultdict(int)
saveRow = [''] * wordCnt

for i in range(wordCnt):
    word = input()
    wordLen = len(word)
    saveRow[i] = word

    for j in range(wordLen):
        digit = str(wordLen - j)
        saveWordDigit[word[j]] += int(f'1{"0"*int(digit)}')

sortWordDigit = sorted(saveWordDigit.items(), reverse=True, key=lambda row:row[1])
conWordDigit = [(sortWordDigit[i][0], str(9-i)) for i in range(len(sortWordDigit))]
wordDigitDict = dict(conWordDigit)

for row in saveRow:
    saveTemp = ''

    for word in row:
        saveTemp += wordDigitDict[word]

    answer += int(saveTemp)

print(answer)