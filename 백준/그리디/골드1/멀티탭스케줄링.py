n, k = map(int, input().split())
schedules = list(map(int, input().split()))
multitap = []
useCnt = 0

for i in range(len(schedules)):
    print('----------------------------------------------------------------')

    if schedules[i] in multitap:
        print(schedules[i], '이미 멀티탭에 있음')
        continue

    if len(multitap) < n:
        print(schedules[i], '멀티탭에 꼽음')
        multitap.append(schedules[i])
        continue

    afterSchedules = schedules[i:]
    saveSchedules = []
    inMulti = True

    for j in range(len(multitap)):  # 더이상 뒤 스케줄에 없는 종류 확인
        idx = 101

        if multitap[j] in afterSchedules:   # 뒤 스케줄에 있는 멀티탭 저장
            saveSchedules.append(afterSchedules.index(multitap[j]))
        else: # 뒤 스케줄에 없는 멀티탭
            inMulti = False
            multitap.remove(multitap[j])
            break

    print('current kinds', schedules[i])
    print(afterSchedules)
    print(saveSchedules)
    if inMulti:
        del multitap[multitap.index(afterSchedules[max(saveSchedules)])]

    print('멀티탭현황', multitap)
    print(schedules[i], '멀티탭에 꼽음')
    multitap.append(schedules[i])
    useCnt += 1

print(useCnt)