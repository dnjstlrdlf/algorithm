import heapq

N, L = map(int, input().split())
repairDistance = list(map(int, input().split()))
heapq.heapify(repairDistance)
endDistance = 0
answer = 0

while repairDistance:
    current = repairDistance[0]
    # print('endDistance', endDistance)
    if endDistance >= current:
        heapq.heappop(repairDistance)
        # print('current', current)
    else:
        answer += 1
        # print('테이프 추가', endDistance, current, current + L - 0.5)
        endDistance = current + L - 0.5


print(answer)