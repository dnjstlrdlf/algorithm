idx = 0

while True:
    L, P, V = map(int, input().split())

    if L == 0 and P == 0 and V == 0:
        break

    divCnt = V // P
    remainder = min(L, V % P)
    answer = L * divCnt + remainder
    idx += 1
    print(f'Case {idx}: {answer}')