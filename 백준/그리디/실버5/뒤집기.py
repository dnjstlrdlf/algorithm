numbers = input()
numLen = len(numbers)
isCheck = False
answer = 0

for i in range(numLen):
    if numbers[i] != numbers[0]:
        if not isCheck:
            answer += 1
        isCheck = True
    else:
        isCheck = False

print(answer)