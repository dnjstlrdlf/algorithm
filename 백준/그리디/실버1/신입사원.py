import sys

caseCnt = int(sys.stdin.readline())

for i in range(caseCnt):
    applicants = int(sys.stdin.readline())
    applicantsList = []
    cnt = 0

    for j in range(applicants):
        docRank, interRank = map(int, sys.stdin.readline().split())
        applicantsList.append({'doc': docRank, 'inter': interRank})

    applicantsList.sort(key=lambda row: row['doc'])
    maxInterRank = applicantsList[0]['inter']
    # print(applicantsList)

    for j in range(1, len(applicantsList)):
        if maxInterRank < applicantsList[j]['inter']:
            cnt += 1

        if maxInterRank > applicantsList[j]['inter']:
            maxInterRank = applicantsList[j]['inter']

    print(applicants - cnt)