from collections import deque

a, b = map(int, input().split())
saveList = deque([[1, a]])
checkSuccess = False
answer = 0

while saveList:
    current = saveList.popleft()
    idx = current[0]+1
    temp = [int(f'{current[1]}1'), current[1]*2]
    # print(current, temp)

    for converse in temp:
        if converse < b:
            saveList.append([idx, converse])
        elif converse == b:
            answer = idx
            checkSuccess = True

    if checkSuccess:
        break


print(answer if checkSuccess else -1)