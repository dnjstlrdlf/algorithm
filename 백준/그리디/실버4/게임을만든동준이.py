loopCnt = int(input())
levList = [int(input()) for i in range(loopCnt)]
answer = 0

for i in range(len(levList)-2, -1, -1):
    nextLevScore = levList[i+1]
    currentLevScroe = levList[i]

    if nextLevScore <= currentLevScroe:
        subScore = currentLevScroe - nextLevScore + 1
        answer += subScore
        levList[i] -= subScore

print(answer)