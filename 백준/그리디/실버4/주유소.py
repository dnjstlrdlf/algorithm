cityCnt = int(input())-1
distanceList = list(map(int, input().split()))
priceList = list(map(int, input().split()))[0:-1]
minPrice = priceList[0]
usePrice = 0

for i in range(cityCnt):
    distance = distanceList[i]
    oilPrice = priceList[i]

    if minPrice > oilPrice:
        minPrice = oilPrice

    usePrice += minPrice * distance

print(usePrice)