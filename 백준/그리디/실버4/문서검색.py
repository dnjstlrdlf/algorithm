totalStr = input()
totalStrLen = len(totalStr)
search = input()
searchLen = len(search)
startIdx = 0
answer = 0

while totalStrLen-1 >= startIdx:
    splitStr = totalStr[startIdx:startIdx+searchLen]

    if search == splitStr:
        answer += 1
        startIdx += searchLen
    else:
        startIdx += 1

print(answer)