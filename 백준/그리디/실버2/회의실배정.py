import sys

n = int(input())
endTime = 0
cnt = 0
saveInfoList = []

for i in range(n):
    start, end = map(int, sys.stdin.readline().split())

    saveInfoList.append({
        'start': start,
        'end': end,
        'duration': end - start
    })

saveInfoList.sort(key=lambda row: (row['end'], row['start']))

for i in range(n):
    if endTime <= saveInfoList[i]['start']:
        endTime = saveInfoList[i]['end']
        cnt += 1
        # print(saveInfoList[i])

print(cnt)