n, k = map(int, input().split())
savePrice = [0] * n
cnt = 0

for i in range(n):
    savePrice[i] = int(input())

for i in range(n-1, -1, -1):
    cnt += k // savePrice[i]
    k = k % savePrice[i]

    if k == 0:
        break

print(cnt)