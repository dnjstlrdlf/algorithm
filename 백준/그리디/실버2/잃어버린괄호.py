formula = input()
isOpenBracket = False
subSave = 0
answer = 0
saveNum = ''

for i in formula:
    if i.isdecimal():
        saveNum += i
    else:
        if isOpenBracket:
            answer -= int(saveNum)
        else:
            answer += int(saveNum)
        saveNum = ''

        if i == '-':
            isOpenBracket = True


if isOpenBracket:
    answer -= int(saveNum)
else:
    answer += int(saveNum)

print(answer)