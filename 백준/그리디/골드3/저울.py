loopCnt = int(input())
weights = list(map(int, input().split()))
weights.sort()

startWeight = 1
answer = 0

while True:
    checkWeight = startWeight
    useWeight = []
    print('startWeight', startWeight)

    for weight in weights:
        if checkWeight >= weight:
            print('subWeight', weight)
            checkWeight -= weight
            useWeight.append(weight)
        else:
            break

    if checkWeight != 0:
        answer = startWeight
        break

    startWeight += 1

print(answer)