r, c = map(int, input().split())
mapList = [list(input()) for i in range(r)]

# 오른쪽 위, 오른쪽 오른쪽 아래 우선순위
# 기준 x+1, y+1 / x+1, y / x+1, y-1
startY, startX = 0, 0
answer = 0

for i in range(r):
    savePath = [[i, 0]]

    while savePath:
        currentPath = savePath.pop()  # [[0,0]]
        x, y = currentPath[1], currentPath[0]
        mapList[y][x] = 'x'

        # 목적지 도착
        if x + 1 == c:
            answer += 1
            break

        # 오른쪽 아래
        if y + 1 < r and mapList[y + 1][x + 1] == '.':
            savePath.append([y + 1, x + 1])

        # 오른쪽
        if mapList[y][x + 1] == '.':
            savePath.append([y, x + 1])

        # 오른쪽 위
        if y - 1 >= 0 and mapList[y - 1][x + 1] == '.':
            savePath.append([y - 1, x + 1])

print(answer)