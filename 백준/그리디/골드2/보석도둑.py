import sys
import heapq

n, k = map(int, sys.stdin.readline().split())
saveGem = []
saveBag = []
answer = 0

for i in range(n):
    m, v = list(map(int, sys.stdin.readline().split()))
    heapq.heappush(saveGem, (m, v))

for i in range(k):
    limit = int(sys.stdin.readline())
    heapq.heappush(saveBag, limit)

currentGemList = []

while saveBag:
    bagWeight = heapq.heappop(saveBag)

    while saveGem and saveGem[0][0] <= bagWeight:
        heapq.heappush(currentGemList, -saveGem[0][1])
        heapq.heappop(saveGem)

    if currentGemList:
        answer -= heapq.heappop(currentGemList)
    elif not saveGem:
        break

print(answer)