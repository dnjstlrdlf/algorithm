wordDict = {'c=': 2, 'c-': 2, 'dz=': 3, 'd-': 2, 'lj': 2, 'nj': 2, 's=': 2, 'z=': 2}
word = input()
wordLen = len(word)
wordStart = 2
wordEnd = 4
currentIdx = 0
cnt = 0

while True:
    useDict = False
    saveTemp = ''

    for i in range(wordStart, wordEnd):
        cutWord = word[currentIdx:currentIdx+i]

        if cutWord in wordDict:
            cnt += 1
            currentIdx += i
            useDict = True

            saveTemp = cutWord
            break

    if not useDict:
        saveTemp = word[currentIdx]
        cnt += 1
        currentIdx += 1

    if currentIdx >= wordLen:
        break

print(cnt)