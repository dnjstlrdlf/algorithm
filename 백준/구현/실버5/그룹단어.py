cnt = int(input())
answer = 0

for idx in range(cnt):
    word = input()
    wordLength = len(word)
    saveUseWord = dict()
    isNotGroupWord = False

    for i in range(wordLength):
        if i >= 1 and word[i] in saveUseWord and word[i-1] != word[i]:
            isNotGroupWord = True
            break
        saveUseWord[word[i]] = 1

    if not isNotGroupWord:
        answer += 1

print(answer)