limit = 10000
saveSelfNum = [False] * (limit+1)

for i in range(1, limit+1):
    currentNum = i

    while True:
        addDigitsOfNum = currentNum
        strCurrent = str(currentNum)

        for digits in strCurrent:
            addDigitsOfNum += int(digits)

        if addDigitsOfNum > limit:
            break

        currentNum = addDigitsOfNum

        if not saveSelfNum[addDigitsOfNum]:
            saveSelfNum[addDigitsOfNum] = True


for i in range(1, len(saveSelfNum)):
    if not saveSelfNum[i]:
        print(i)