words = input()
wordsLen = len(words)
openBracket = False
answer = ''
temp = ''


def word_reverse(word):
    split_word = word.split()
    save_result = ''

    for word in split_word:
        temp_list = list(word)
        temp_list.reverse()
        save_result += ''.join(temp_list) + ' '

    return save_result[0:-1]


for i in range(wordsLen):
    current = words[i]

    if current == '<':
        if temp:
            answer += word_reverse(temp)
            temp = ''
        openBracket = True
        answer += current
    elif current == '>':
        openBracket = False
        answer += current
    elif openBracket:
        answer += current
    else:
        temp += current

if temp:
    answer += word_reverse(temp)
    temp = ''

print(answer)