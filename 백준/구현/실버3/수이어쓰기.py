n = int(input())
idx = 0
numDigits = 0
temp = 0

while True:
    limit = int("9"+f'{"0"*idx}')
    digits = limit * (idx+1)

    if n <= limit:
        break

    numDigits += digits
    idx += 1

nSubLimit = (n - (int(f'{"9"*idx}') if "9"*idx != "" else 0)) * (idx+1)
numDigits += nSubLimit
print(numDigits)




limit = int(input())
length = 0
for i in range(len(str(limit))):
    numLen = int('9' + '0'*i)
    num = int('9' * (i+1))
    if limit > num:
        length += (numLen * (i+1))
    else:
        if i == 0:
            length += (limit * (i+1))
        else:
            length += ((limit - int('9' * i)) * (i+1))
print(length)