n, m = map(int, input().split())
minLen = min(n, m)
rectangle = []
answer = 1
isStop = False

for i in range(n):
    rectangle.append(list(input()))

for i in range(minLen-1, -1, -1):
    limitLen = i
    # print(n, limitLen)
    for j in range(0, n-limitLen):
        # print('j', rectangle[j])
        for k in range(0, m-limitLen):
            leftUp = rectangle[j][k]
            rightUp = rectangle[j][k+limitLen]
            leftDown = rectangle[j+limitLen][k]
            rightDown = rectangle[j+limitLen][k+limitLen]

            if leftUp == rightUp and rightUp == leftDown and leftDown == rightDown:
                # print(leftUp, rightUp, leftDown, rightDown, limitLen)
                answer = (limitLen+1)**2
                isStop = True
                break

        if isStop:
            break
    if isStop:
        break

print(answer)