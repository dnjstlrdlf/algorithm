from collections import deque

row = input()
stack = deque(row)
saveStack = deque()
answer = 0
currentMulti = 1
isAdd = False

if len(row) % 2 == 1:
    print(0)
else:
    while stack:
        lastBracket = stack.pop()

        if lastBracket == ')' or lastBracket == ']':
            saveStack.append(lastBracket)
            multiVal = 3 if lastBracket == ']' else 2
            currentMulti *= multiVal
            isAdd = True

        elif saveStack:
            lastSaveBracket = saveStack.pop()

            if lastBracket == '(' and lastSaveBracket == ')':
                if isAdd:
                    answer += currentMulti
                currentMulti //= 2
                isAdd = False
            elif lastBracket == '[' and lastSaveBracket == ']':
                if isAdd:
                    answer += currentMulti
                currentMulti //= 3
                isAdd = False
            else:
                answer = 0
                break

        else:
            answer = 0
            break

    print(answer)