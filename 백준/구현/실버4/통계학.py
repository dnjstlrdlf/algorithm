import sys
from collections import Counter

loopCnt = int(input())
saveNumArr = [0] * loopCnt

for loop in range(loopCnt):
    inputNum = int(sys.stdin.readline())
    saveNumArr[loop] = inputNum

saveNumArr.sort()
average = int(round(sum(saveNumArr) / loopCnt, 0))
print(average)

print(saveNumArr[loopCnt//2])

tempArr = Counter(saveNumArr).most_common(2)
print(tempArr[1][0] if len(tempArr) > 1 and tempArr[0][1] == tempArr[1][1] else tempArr[0][0])
print(saveNumArr[loopCnt-1] - saveNumArr[0])