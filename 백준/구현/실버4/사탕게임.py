rowCnt = int(input())
saveRow = []
cnt = 0


def color_print(rows):
    for row in rows:
        print(row)


def color_check(rows):
    global cnt
    # 가로체크
    for y in range(len(rows)):
        bef_color = ''
        save_cnt = 0
        color_cnt = 0

        for x in range(len(rows[0])):
            if bef_color != rows[y][x]:
                bef_color = rows[y][x]
                color_cnt = 1
            else:
                color_cnt += 1

                if color_cnt > save_cnt:
                    save_cnt = color_cnt

        if cnt < save_cnt:
            cnt = save_cnt

    for x in range(len(rows[0])):
        bef_color = ''
        save_cnt = 0
        color_cnt = 0

        for y in range(len(rows)):
            if bef_color != rows[y][x]:
                bef_color = rows[y][x]
                color_cnt = 1
            else:
                color_cnt += 1

                if color_cnt > save_cnt:
                    save_cnt = color_cnt

        if cnt < save_cnt:
            cnt = save_cnt


def color_change(rows, y, x):
    # left
    print(y, x)
    if x-1 >= 0 and rows[y][x] != rows[y][x-1]:
        rows[y][x], rows[y][x-1] = rows[y][x-1], rows[y][x]
        color_print(rows)
        color_check(rows)
        rows[y][x], rows[y][x - 1] = rows[y][x - 1], rows[y][x]
        print('left END ---------------------------------------------------------')
    # right
    if x+1 <= len(rows[y])-1 and rows[y][x] != rows[y][x+1]:
        rows[y][x], rows[y][x+1] = rows[y][x+1], rows[y][x]
        color_print(rows)
        color_check(rows)
        rows[y][x], rows[y][x + 1] = rows[y][x + 1], rows[y][x]
        print('right END---------------------------------------------------------')
    # up
    if y-1 >= 0 and rows[y-1][x] != rows[y][x]:
        rows[y-1][x], rows[y][x] = rows[y][x], rows[y-1][x]
        color_print(rows)
        color_check(rows)
        rows[y - 1][x], rows[y][x] = rows[y][x], rows[y - 1][x]
        print('up END---------------------------------------------------------')
    # down
    if y+1 <= len(rows)-1 and rows[y+1][x] != rows[y][x]:
        rows[y+1][x], rows[y][x] = rows[y][x], rows[y+1][x]
        color_print(rows)
        color_check(rows)
        rows[y + 1][x], rows[y][x] = rows[y][x], rows[y + 1][x]
        print('down END---------------------------------------------------------')


for i in range(rowCnt):
    saveRow.append(list(input()))


for i in range(rowCnt):
    xLen = len(saveRow[i])
    for j in range(xLen):
        color_change(saveRow, i, j)

print(cnt)
