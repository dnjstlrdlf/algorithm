a, b = input().split()
bLen = len(b)
aLen = len(a)
sub = bLen-aLen+1
answer = aLen

for i in range(sub):
    cnt = 0
    bSplit = b[0+i:aLen+i]
    bSplitLen = len(bSplit)

    for j in range(bSplitLen):
        if a[j] != bSplit[j]:
            cnt += 1

    if answer > cnt:
        answer = cnt

print(answer)