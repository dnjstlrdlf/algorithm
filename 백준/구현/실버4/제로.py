loopCnt = int(input())
savePrices = 0

for loop in range(loopCnt):
    price = int(input())

    if price != 0:
        savePrices.append(price)
    else:
        savePrices.pop()

print(sum(savePrices))