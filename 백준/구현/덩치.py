cnt = int(input())
saveInfo = []

for i in range(cnt):
    kg, cm = map(int, input().split())
    saveInfo.append({'kg': kg, 'cm': cm, 'idx': i})


for i in range(cnt):
    current = saveInfo[i]
    saveCnt = 1

    for j in range(cnt):
        temp = saveInfo[j]

        if i != j:
            if current['kg'] < temp['kg'] and current['cm'] < temp['cm']:
                saveCnt += 1

    saveInfo[i]['rank'] = saveCnt

saveInfo.sort(key=lambda row: row['idx'])

for row in saveInfo:
    print(row['rank'], end=' ')